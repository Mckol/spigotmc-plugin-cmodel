package com.mckol.cmodel.commands

import dev.jorel.commandapi.CommandAPI
import org.bukkit.entity.Player

class CustomModelDataCommand {
    fun onGet(sender: Player, args: Array<out Any>): Int {
        handleAir(sender) ?: return 0
        val meta = sender.inventory.itemInMainHand.itemMeta
        if (meta != null) {
            if (meta.hasCustomModelData()) {
                sender.sendMessage("The item's custom model data is: \"${meta.customModelData}\"")
            } else {
                sender.sendMessage("The item has no custom model data.")
            }
        } else {
            CommandAPI.fail("Couldn't get item data.")
        }
        return 1
    }

    fun onSet(sender: Player, args: Array<out Any>): Int {
        handleAir(sender) ?: return 0
        val model = args[0] as Int
        return setModelData(sender, model, "Custom model data set to: \"${model}\"")
    }

    fun onClear(sender: Player, args: Array<out Any>): Int {
        handleAir(sender) ?: return 0
        return setModelData(sender, null, "Custom model data cleared.")
    }

    /**
     * Sets the custom model data for the item held in the main hand.
     *
     * @return 1 on success, 0 on failure (if ItemStack's ItemMeta was null).
     * @param player The player instance to modify.
     * @param model The custom model data integer to set. Pass null to clear the data instead.
     * @param message The message to send to the Player on success
     */
    private fun setModelData(player: Player, model: Int?, message: String): Int {
        val inv = player.inventory
        val item = inv.itemInMainHand
        val meta = item.itemMeta
        if (meta == null) {
            CommandAPI.fail("Internal error: itemMeta was null. Please report this to the plugin developer.")
            return 0
        }

        meta.setCustomModelData(model)
        item.itemMeta = meta
        inv.setItemInMainHand(item)

        player.sendMessage(message)

        return 1
    }

    /**
     * Handles the case when player doesn't have any item in their main hand
     *
     * @return null if the held item is air (for easy handling with the elvis operator)
     * @param sender The player whose main hand to check.
     */
    private fun handleAir(sender: Player): Unit? {
        if (sender.inventory.itemInMainHand.type.isAir) {
            CommandAPI.fail("You must be holding an item in your main hand to use this command.")
            return null
        }
        return Unit
    }

}
